<script src="http://miami.edu/index.php/_javascripts/jquery-1.2.3.pack" language="javascript"></script>

<!-- This Javascript should be on the same page where the Momentum 2 banner is going to be placed-->
<script type="text/javascript">
$(function() {
	$('ul.hover_block2 li').hover(function(){
		$(this).find('#M2up').animate({left:'195px'},{queue:false,duration:500});
	}, function(){
		$(this).find('#M2up').animate({left:'0px'},{queue:false,duration:500});
	});
});
</script>

<style media="screen">
ul.hover_block2 li {
	width: 197px; 
	padding: 0px;
	top: 0px;
	left: 0px;
	list-style: none;
	position: absolute;
	background: transparent;
	border: 0px;	
	margin-right: 0px; }

ul.hover_block2 li a {
	width: 197px;
	height: 25px;
	padding: 0px; }

ul.hover_block2 li a { 
	text-decoration: none;
	background: none; }

ul.hover_block2 li img {
	width: 197px;
	height: 25px;
	position: absolute;
	top: 0;
	left: 0;
	border: 0; }
	
.mom2 {
	width: 197px;
	height: 25px;	
	padding-bottom: 0px;
	border: 0px solid #F47321; }
</style>  

<div class="top-nav">

<!-- START MOMENTUM 2 AD-->
<ul class="hover_block2">
<li><a href="http://www.miami.edu/momentum2" target="blank"><img id="over" style="left: 0px;" src="http://www.law.miami.edu/img/momentum2_over.png" alt="Momentum2"><img id="M2up" style="left: 0px;" src="http://www.law.miami.edu/img/momentum2_darkgreen.png" alt="Momentum2"></a></li>
</ul>
<!-- END MOMENTUM 2 AD-->

<ul>
<li><a href="http://www.miami.edu/">UM Home</a></li>
<li><a href="https://madison.law.miami.edu/webinquiry/inquiry.asp" target="_blank">Request Info</a></li>
<li><a href="/calendar/">Calendars</a></li>
<li><a href="/telephone-directory/">Directory</a></li>
<li><a href="/alumni/giving.php?op=1"><font class="giving-orange">Support Miami Law</font></a></li>
<li class="last">Search:</li>

<!--START SEARCH BOX-->
<li class="search">
<form id="searchbox_003627511607479437825:ntpyqqxef0m" name="search" action="http://www.law.miami.edu/search.php">
<fieldset>
<p>
<input type="hidden" name="cx" value="003627511607479437825:ntpyqqxef0m" />
<input type="hidden" name="cof" value="FORID:11" />
<input type="text" name="q" value="" class="input-text" id="textfield" onkeypress="checkEnter(event)" onclick="select()"/>
<input type="image" src="http://www.law.miami.edu/_ui/images/bt-search.png" id="bt-search" value="Go" />
</p>
</fieldset>
</form>

<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=searchbox_003627511607479437825%3Antpyqqxef0m&amp;lang=en"></script>
</li>
</ul>
<!--END SEARCH BOX-->      
	    
</div>