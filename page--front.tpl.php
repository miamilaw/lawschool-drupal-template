<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div class="content-wrap">
    <div class="content-inside">
        <div id="page-wrapper">
            <div id="page">

                <div class="container">

                    <!--
                    <div class="top-nav">-->
                        <?php //if ($page['top_ad_block']): ?>
                            <?php //print render($page['top_ad_block']); ?>
                        <?php //endif; ?>
                        <?php //if ($page['top_navigation']): ?>
                            <?php //print render($page['top_navigation']); ?>
                        <?php //endif; ?>
                    <!--
                    </div>
                    -->
                    <?php include_once('top_navigation.php'); ?>
                    <div class="header">
                        <div class="toplogo">
                            <div id="header">
                                <div class="section clearfix">
                                    <?php if ($logo): ?>
                                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"
                                           id="logo">
                                            <img src="<?php print base_path() . path_to_theme(); ?>/img/logo.gif"
                                                 alt="<?php print t('Home'); ?>"/>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php include_once('main_navigation.php'); ?>

                        <?php //if ($page['main_navigation']): ?>
                            <?php //print render($page['main_navigation']); ?>
                        <?php //endif; ?>

                    </div>

                    <div class="content-main">
                        <div class="content">

                            <div class="flash-top">
                            </div>
                            <div class="announcement1">
                                <div class="announcement1-content">

                                    <img src="http://www.law.miami.edu/img/homepage/hdr-faculty-focus.png" title="Miami Law: Faculty Focus" alt="Miami Law: Faculty Focus">

                                    <br>

                                    Professor <b>Anthony Alfieri</b> Publishes Article on Civil Rights and Environmental Justice &nbsp; <a href="/faculty-administration/index.php?op=0">Read more</a>

                                    <br><br>

                                    <b>Scott Rogers</b> Publishes Research Article on Short-Form Mindfulness Training and Mind Wandering &nbsp; <a href="/faculty-administration/index.php?op=0">Read more</a>

                                    <br><br>

                                    Professor <b>Stephen Urice</b> Participates in Miami’s Art Basel &nbsp; <a href="/faculty-administration/index.php?op=0">Read more</a>

                                </div>
                            </div>
                            <div class="announcement2">


                                <div class="announcement2-content">

                                    <div class="announcement2-left">
                                        <img src="http://www.law.miami.edu/img/homepage/img-why-miami-2.png" title="Heading into downtown Miami" alt="Heading into downtown Miami">
                                    </div>

                                    <div class="announcement2-right">
                                        <img src="http://www.law.miami.edu/img/homepage/hdr-why-miami.png" title="Miami Law: Why Study Law In Miami?" alt="Miami Law: Why Study Law In Miami?">

                                        <p><b>Miami means business:</b> Miami serves as the international headquarters for many Fortune 500 corporations, including UPS, Sony, The Walt Disney Company and Ryder. <a href="/prospective-students/why-study-in-miami.php?op=2">Learn more</a></p>
                                    </div>

                                </div>

                            </div>
                            <div class="announcement3">


                                <p><img src="http://www.law.miami.edu/img/homepage/hdr-video-hm.png" title="Miami Law: Video" alt="Miami Law: Video"></p>

                                <iframe src="//www.youtube.com/embed/p-6z29lr88k?list=PL5qQGzdjNpDjaPLy1B5H_QZIj38dzpbMG" allowfullscreen="" frameborder="0" height="239" width="425"></iframe>

                                <p>Becky Greenfield, Class of 2015, talks about wanting to study public health policy and how this combination of degrees will give her the background necessary for her career goals. <a href="http://www.law.miami.edu/prospective-students/student-spotlights.php?op=0 ">Watch More Student Spotlights. </a>

                                    <!--<embed width="425" height="239" flashvars="&author=University%20of%20Miami%20-%20School%20of%20Law&bandwidth=95003&date=0&description=0&dock=false&file=20130924_RecruitmentInterview15.mp4&image=%2Fvideo%2Fimg%2Fimg-homepage-111213.jpg&level=0&plugins=viral-2d&streamer=rtmp%3A%2F%2Fvideo.law.miami.edu%2FSpringOrientation2010&title=Miami%20Law%20Video" allowfullscreen="true" allowscriptaccess="always" src="/media/jw_player/player-viral.swf"></embed>

                                    <p>"Never underestimate the power of your network" says <b>3L Shannon Williams</b>, who talks about the summer jobs at a commercial litigation firm and with a U.S. Magistrate Judge. <a href="http://www.law.miami.edu/prospective-students/student-spotlights.php?op=0" target="_blank">Watch More Student Spotlights</a>.</p>-->

                                    <!--<iframe width="425" height="239" src="//www.youtube.com/embed/JPhfrtdO8XA?list=UU40ObcLex2Y1mvuScJGW1Lw" frameborder="0" allowfullscreen></iframe>

                                    <p><b>Orientation 2013 &ndash; HOPE Public Interest Day of Service</b>: Visit <a href="http://www.law.miami.edu/hope/">www.law.miami.edu/hope</a> for more information. Hear interviews with students from our HOPE Public Interest Day of Service, an amazing way we kick off orientation each year. On this day, Miami Law's Public Interest Resource Center provides opportunities for new and returning students to participate in pro bono and community service events at 14 different sites across Miami-Dade County.</p>-->

                                    <!--<embed width="425" height="239" flashvars="&author=University%20of%20Miami%20-%20School%20of%20Law&bandwidth=95003&date=0&description=0&dock=false&file=20130815_HOPEDayofService.mp4&image=%2Fvideo%2Fimg%2Fimg-homepage-100713.jpg&level=0&plugins=viral-2d&streamer=rtmp%3A%2F%2Fvideo.law.miami.edu%2FSpringOrientation2010&title=Miami%20Law%20Video" allowfullscreen="true" allowscriptaccess="always" src="/media/jw_player/player-viral.swf"></embed>-->

                                </p>
                            </div>
                        </div>
                        <div class="aside">

                            <div class="bricks-wrap">
                                <div class="bricks-content">

                                    <div class="bricks-box news-events">
                                        <div class="news">

                                            <img src="http://www.law.miami.edu/img/homepage/hdr-headlines.png" title="Miami Law: Headlines" alt="Miami Law: Headlines">

                                            <br>

                                            <h2><a href="http://www.law.miami.edu/news/2014/january/2706.php">LawWithoutWalls Kicks Off in Switzerland; Plus Expands with All-Virtual Model "LWOW X"</a></h2>

                                            <p><img src="http://www.law.miami.edu/news/img/thumbs/2014/homepage-2707a.jpg" class="news-thumb" alt="Immigration Clinic Students Halt Deportation of Torture Victim" title="Immigration Clinic Students Halt Deportation of Torture Victim"><span class="news-tag">CLINIC SUCCESS</span><br><a href="/news/2014/january/2707.php">Immigration Clinic Students Halt Deportation of Torture Victim</a><br><br></p>

                                            <p><img src="http://www.law.miami.edu/news/img/thumbs/2014/homepage-2708a.jpg" class="news-thumb" alt="Miami Law to Host One-Week Course on Fundamentals of International Arbitration" title="Miami Law to Host One-Week Course on Fundamentals of International Arbitration"><span class="news-tag">SHORT COURSE</span><br><a href="/news/2014/january/2708.php">Miami Law to Host One-Week Course on Fundamentals of International Arbitration</a><br><br></p>

                                            <p><img src="http://www.law.miami.edu/news/img/thumbs/2014/homepage-2709a.jpg" class="news-thumb" alt="Attorney Arlene Zalayet, JD ’81, to Teach About Preparing the Corporate Client for Litigation" title="Attorney Arlene Zalayet, JD ’81, to Teach About Preparing the Corporate Client for Litigation"><span class="news-tag">VISITING FACULTY</span><br><a href="/news/2014/january/2709.php">Attorney Arlene Zalayet, JD ’81, to Teach About Preparing the Corporate Client for Litigation</a><br><br></p>

                                            <p><a href="#"><b>More News</b></a> &nbsp; | &nbsp; <a href="#"><b>Media Inquiries</b></a></p>

                                        </div>
                                        <div class="events">

                                            <div class="events-header">
                                                <img src="http://www.law.miami.edu/img/homepage/hdr-events.png" title="Miami Law: Events" alt="Miami Law: Events">
                                            </div>

                                            <!--START EVENT #1-->
                                            <div class="events-left">

                                                <p><span class="events-date">January 20 - 24</span><br>
                                                    <a href="/pdf/events/2014/012014-diversity-week.jpg" target="blank">Diversity Week</a><br>Miami Law Campus<br>
                                                    <!--<a href="https://docs.google.com/forms/d/17QF3GUmAXlbui6Svh8O7z-mdAmcN-tL9i1Uz-2bxBRA/viewform" target="_blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a> &nbsp;--><a href=" https://maps.google.com/maps?q=1330+Miller+Drive,+Miami,+FL&amp;hl=en&amp;sll=25.720233,-80.279508&amp;sspn=0.003804,0.004823&amp;hnear=1330+Miller+Dr,+Miami,+Florida+33146&amp;t=m&amp;z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>

                                                <p><span class="events-date">January 21 - 25</span><br>
                                                    <a href="/international-graduate-law-programs/international-arbitration/fundamentals.php?op=3
">International Arbitration Course</a><br>Miami Law Campus<br>
                                                    <!--<a href="https://docs.google.com/forms/d/17QF3GUmAXlbui6Svh8O7z-mdAmcN-tL9i1Uz-2bxBRA/viewform" target="_blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a> &nbsp;--><a href=" https://maps.google.com/maps?q=1330+Miller+Drive,+Miami,+FL&amp;hl=en&amp;sll=25.720233,-80.279508&amp;sspn=0.003804,0.004823&amp;hnear=1330+Miller+Dr,+Miami,+Florida+33146&amp;t=m&amp;z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>

                                                <p><span class="events-date">January 27</span><br>
                                                    <a href="/pdf/events/2014/012714-clinics.jpg" target="blank">Clinics Open House</a><br>Miami Law Campus<br>
                                                    <!--<a href="https://docs.google.com/forms/d/17QF3GUmAXlbui6Svh8O7z-mdAmcN-tL9i1Uz-2bxBRA/viewform" target="_blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a> &nbsp;--><a href="https://maps.google.com/maps?q=1330+Miller+Drive,+Miami,+FL&amp;hl=en&amp;sll=25.720233,-80.279508&amp;sspn=0.003804,0.004823&amp;hnear=1330+Miller+Dr,+Miami,+Florida+33146&amp;t=m&amp;z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>

                                            </div>

                                            <div class="events-right">

                                                <p><span class="events-date">January 28</span><br>
                                                    <a href="/pdf/events/2014/012814-businessofhealthlaw.jpg" target="blank">The Business of Health Law</a><br>Student Activities Center<br>
                                                    <!--<a href="https://docs.google.com/forms/d/17QF3GUmAXlbui6Svh8O7z-mdAmcN-tL9i1Uz-2bxBRA/viewform" target="_blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a> &nbsp;--><a href="https://maps.google.com/maps?q=1330+Miller+Drive,+Miami,+FL&amp;hl=en&amp;sll=25.720233,-80.279508&amp;sspn=0.003804,0.004823&amp;hnear=1330+Miller+Dr,+Miami,+Florida+33146&amp;t=m&amp;z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>

                                                <p><span class="events-date">January 29</span><br>
                                                    <a href="/pdf/events/2014/012914-ameri-constitution-federalist-society-roe-wade.jpg" target="blank">Has <i>Roe v. Wade</i> Been<br>Good to Women?</a><br>Student Activities Center<br>
                                                    <!--<a href="https://docs.google.com/forms/d/17QF3GUmAXlbui6Svh8O7z-mdAmcN-tL9i1Uz-2bxBRA/viewform" target="_blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a> &nbsp;--><a href="https://maps.google.com/maps?q=1330+Miller+Drive,+Miami,+FL&amp;hl=en&amp;sll=25.720233,-80.279508&amp;sspn=0.003804,0.004823&amp;hnear=1330+Miller+Dr,+Miami,+Florida+33146&amp;t=m&amp;z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>

                                                <p><span class="events-date">January 30</span><br>
                                                    <a href="/communications/evites/2014/robert-tobiassen-lecture-013014.html
" target="blank">Robert Tobiassen Lecture</a><br>Miami Law Campus<br>
                                                    <!--<a href="https://docs.google.com/forms/d/17QF3GUmAXlbui6Svh8O7z-mdAmcN-tL9i1Uz-2bxBRA/viewform" target="_blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a> &nbsp;--><a href="https://maps.google.com/maps?q=1330+Miller+Drive,+Miami,+FL&amp;hl=en&amp;sll=25.720233,-80.279508&amp;sspn=0.003804,0.004823&amp;hnear=1330+Miller+Dr,+Miami,+Florida+33146&amp;t=m&amp;z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>

                                            </div>

                                            <div style="clear: both;">&nbsp;</div>

                                            <div class="events-footer">
                                                <p><a href="https://ems-01.law.miami.edu/MasterCalendar/MasterCalendar.aspx"><b>All Events Calendar</b></a></p>

                                                <br>

                                            </div>
                                        </div>

                                        <!--EVENTS TEMPLATES HERE-->

                                        <!--
                                        <p><span class="events-date">FEBRUARY 1</span><br />
                                        <a href="/pdf/events/2013/FILENAME GOES HERE.pdf" target="blank">Speaker Series: Race, Gender & Class at Work</a>, Prof. Tomiko Brown-Nagin<br />
                                        <a href="mailto:ddavis@law.miami.edu?subject=Speaker Series Race, Gender and Class at Work RSVP"><img src="http://www.law.miami.edu/img/homepage/btn-events-rsvp-hm.png" class="events-thumb"></a><img src="http://www.law.miami.edu/img/homepage/btn-invisible-events-and-map.png" class="events-thumb"> &nbsp; <a href="https://maps.google.com/maps?q=1311+Miller+Drive,+Coral+Gables,+FL&hl=en&sll=25.76682,-80.19062&sspn=0.008541,0.013937&oq=1311+miller+dri&hnear=1311+Miller+Dr,+Miami,+Miami-Dade,+Florida+33146&t=m&z=17" target="blank"><img src="http://www.law.miami.edu/img/homepage/btn-events-map-hm.png" class="events-thumb"></a></p>
                                        -->
                                        <div class="fyi">

                                            <img src="http://www.law.miami.edu/img/homepage/hdr-fyi.png" title="Miami Law: FYI" alt="Miami Law: FYI">

                                            <br>

                                            <p><a href="/academics/converge/">Converge! Re-Imagining the Movement to End Gender Violence</a></p>

                                            <p><a href="/prospective-students/recruiting-events-calendar.php?op=10">Fall 2013 Recruiting Calendar</a></p>

                                        </div></div>

                                    <span class="bricks-bottom-alt"></span></div>
                            </div>

                            <span class="bricks-bottom"></span>
                        </div>
                    </div>
                    <!-- /.section, /#footer -->
                </div>

                <div style="clear:both;"></div>

                <div class="footer-wrap">
                    <div class="footer">
                        <div class="footer-content">

                            <!--START FOOTER-LAYER1-->
                            <div class="footer-layer1">

                                <div class="footer-layer1-no1">
                                    <?php if ($page['footer_column1']): ?>
                                        <?php print render($page['footer_column1']); ?>
                                    <?php endif; ?>
                                </div>

                                <div class="footer-layer1-no2">
                                    <?php if ($page['footer_column2']): ?>
                                        <?php print render($page['footer_column2']); ?>
                                    <?php endif; ?>
                                </div>

                                <div class="footer-layer1-no3">
                                    <?php if ($page['footer_column3']): ?>
                                        <?php print render($page['footer_column3']); ?>
                                    <?php endif; ?>
                                </div>

                                <div class="footer-layer1-no4">
                                    <?php if ($page['footer_column4']): ?>
                                        <?php print render($page['footer_column4']); ?>
                                    <?php endif; ?>
                                </div>

                            </div>
                            <!--END FOOTER-LAYER1-->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page, /#container -->

        </div> <!-- /#page-wrapper -->
    </div>
</div>