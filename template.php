<?php

function lawschool_preprocess_html(&$variables){
    // Stylesheet
    $options = array(
        'weight' => CSS_THEME,
    );
    if (drupal_is_front_page()){
        $variables['classes_array'][] = 'home';
        drupal_add_css(path_to_theme() . '/css/style_hm.css', $options);
    } else {
        drupal_add_css(path_to_theme() . '/css/style_interior.css', $options);
        drupal_add_css(path_to_theme() . '/css/style_rightrail.css', $options);
    }

    // Javascript
    $options = array(
        'group' => JS_THEME,
    );
    //drupal_add_js('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', $options);
    //drupal_add_js('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', $options);
}

function lawschool_preprocess_page(&$vars, $hook){
    if (isset($vars['node'])){
        if ($vars['node']->type == 'page'){
            $node = node_load($vars['node']->nid);

            // Banner
            $banner = field_view_field('node', $node, 'field_banner', array('label' => 'hidden'));
            $vars['banner'] = $banner;

            // Right content
            $right = field_view_field('node', $node, 'field_right_content', array('label' => 'hidden'));
            $vars['right_content'] = $right;
        }
    }
}

function lawschool_menu_tree__menu_left_menu(array $variables){
    return '<div class="leftnav"><div class="cs_left_nav2"><div class="pad_csnav">' . $variables['tree'] . '</div></div></div>';
}

function lawschool_menu_link__menu_left_menu(array $variables) {
    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
        foreach ($element['#below'] as $elem){
            /*echo '<pre>';
            var_dump($elem["#original_link"]);
            echo '</pre>';*/
            if (isset($elem["#href"]) && isset($elem["#title"])){
                $classdata = 'submenu';
                if (isset($elem["#original_link"]) && isset($elem["#original_link"]["in_active_trail"]) && $elem["#original_link"]["in_active_trail"]){
                    $classdata .= ' active';
                }
                $sub_menu .= '<a class="' . $classdata . '" href="' . url($elem["#href"]) . '">'.$elem["#title"].'</a>';
            }
        }
    }
    /*$output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<a class="menu1">' . $output . $sub_menu . "</a>\n";*/
    /*return '<a class="menu1"' . 'href ="' . url($element['#href']) . '">' . $element['#title'] . '</a>';*/
    $element['#localized_options']['attributes']['class'] = array('menu1');
    return l($element['#title'], $element['#href'], $element['#localized_options']) . '<div class="hide">'.$sub_menu.'</div>';
}

?>
