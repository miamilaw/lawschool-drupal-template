<link href="http://www.law.miami.edu/_ui/skins/black.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type='text/javascript' src='http://www.law.miami.edu/js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='http://www.law.miami.edu/js/jquery.dcmegamenu.1.3.2.js'></script>

<script type="text/javascript">
$(document).ready(function($){
	$('#mega-menu-1').dcMegaMenu({
		rowItems: '5',
		speed: 'fast',
		effect: 'fade'
	});
});
</script>

<div class="demo-container">
<div class="black">

<ul id="mega-menu-1" class="mega-menu">

<!--BUTTON 1 START-->

<li><a href="/academics" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-academics-programs.png" border="0"></a>
<ul>
<li><span class="sectionhdr">DEGREES & COURSES</span>
<ul class="mm-link">
<li><a href="/juris-doctor">J.D. - Juris Doctor</a></li>
<li><a href="/joint-degrees">Joint Degree Programs</a></li>
<li><a href="/llm/?op=0">LL.M. - Master of Laws</a></li>
<li><a href="certificate-programs">Certificate Programs</a></li>
<li><a href="https://lawapps2.law.miami.edu/clink/search.aspx" target="blank">Course Descriptions</a></li>
<li><a href="/academics"><i>More Degrees & Courses</i></a></li>
</ul>
</li>

<li><span class="sectionhdr">CLINICS/PUBLIC INTEREST</span>
<ul class="mm-link">
<li><a href="#">Center for Ethics and Public Service</a></li>
<li><a href="#">Clinics</a></li>
<li><a href="#">HOPE Public Interest Resource Center</a></li>
<li><a href="#">Miami STREET LAW</a></li>
</ul>
</li>

<li><span class="sectionhdr">PROGRAMS</span>
<ul class="mm-link">
<li><a href="#">Externship Programs</a></li>
<li><a href="#">International Graduate Law Programs (IGLP)</a></li>
<li><a href="#">LawWithoutWalls</a></li>
<li><a href="#">Legal Communications and Research Skills (LComm)</a></li>
</ul>
</li>

<li><span class="sectionhdr">&nbsp;</span>
<ul class="mm-link">
<li><a href="#">Litigation Skills Program</a></li>
<li><a href="#">Summer Abroad</a></li>
<li><a href="#">Int'l Exchange Programs</a></li>
<li><a href="#"><i>More Programs</i></a></li>
</ul>
</li>

<li><span class="sectionhdr">FACULTY & ACADEMIC LIFE</span>
<ul class="mm-link">
<li><a href="#">Faculty Profiles</a></li>
<li><a href="#">Lectures & Academic Events at Miami Law</a></li>
<li><a href="#">Law Reviews & Journals</a></li>
<li><a href="#"><i>More</i></a></li>
</ul>
</li>

</ul>
</li>

<!--BUTTON 1 END-->

<!--BUTTON 2 START-->

<li><a href="#" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-prospective-students.png" border="0"></a>
<ul>
<li><span class="sectionhdr">DISCOVER MIAMI LAW</span>
<ul class="mm-link">
<li><a href="#">Admissions Events Calendar</a></li>
<li><a href="https://madison.law.miami.edu/webinquiry/inquiry.asp">Request Information</a></li>
<li><a href="#">Visit Campus</a></li>
<li><a href="#">Class Profile - Fast Facts</a></li>
<li><a href="#">Meet/Contact Recruitment Team</a></li>
<li><a href="#">Applying for Financial Aid</a></li>
<li><a href="#">Scholarships & Grants</a></li>
<li><i><a href="#">More</a></i></li>
</ul>
</li>

<li><span class="sectionhdr">JD ADMISSIONS</span>
<ul class="mm-link">
<li><a href="#">JD Application</a></li>
<li><a href="#">Transfer Students</a></li>
<li><a href="#">Visiting Students</a></li>
<li><a href="#">International Students</a></li>
<li><a href="#">Joint Degrees</a></li>
</ul>
</li>

<li><span class="sectionhdr">LL.M. ADMISSIONS</span>
<ul class="mm-link">
<li><a href="#">Estate Planning</a></li>
<li><a href="#">International Law</a></li>
<li><a href="#">Ocean & Coastal Law</a></li>
<li><a href="#">Real Property Development</a></li>
<li><a href="#">Taxation</a></li>
<li><a href="#">Taxation of Cross-Border Investment</a></li>
<li><i><a href="#">More on LL.M. Programs</a></i></li>
</ul>
</li>

<li><span class="sectionhdr">STUDENT LIFE</span>
<ul class="mm-link">
<li><a href="#">Virtual Tour</a></li>
<li><a href="#">Why Miami?</a></li>
<li><a href="#">Student/Alumni Profiles</a></li>
<li><a href="#">Housing</a></li>
<li><a href="#">Student Organizations</a></li>
<li><a href="#">Student Services & Support</a></li>
</ul>
</li>

<li><span class="sectionhdr">HIGHLIGHTS</span>
<ul class="mm-link">
<li><a href="#">Clinics</a></li>
<li><a href="#">Concentrations</a></li>
<li><a href="#">Externships</a></li>
<li><a href="#">Interdisciplinary Programs</a></li>
<li><a href="#">Law Reviews</a></li>
<li><a href="#">Moot Court/Trial Skills</a></li>
<li><a href="#">Public Interest Programs</a></li>
<li><a href="#">Study Abroad</a></li>
<li><a href="#"><i>More Academics & Programs</i></a></li>
</ul>
</li>

</ul>
</li>

<!--BUTTON 2 END-->

<!--BUTTON 3 START-->

<li><a href="/current-students/?op=0" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-students.png" border="0"></a>
<ul>
<li><span class="sectionhdr">DEGREES & PROGRAMS</span>
<ul class="mm-link">
<li><a href="#">Programs/Degrees</a></li>
<li><a href="#">Course Descriptions</a></li>
<li><a href="#">Grades</a></li>
<li><a href="#">Exams</a></li>
<li><a href="#">Registrar</a></li>
<li><a href="#"><i>More ...</i></a></li>
</ul>
</li>

<li>
<span class="sectionhdr">STUDENT LIFE/SERVICES</span>
<ul class="mm-link">
<li><a href="#">Academic Achievement Program</a></li>
<li><a href="#">Disability Services</a></li>
<li><a href="#">Financial Aid</a></li>
<li><a href="#">Master Events Calendar</a></li>
<li><a href="#">Student Orgs/Journals</a></li>
</ul>
</li>

<li>
<span class="sectionhdr">&nbsp;</span>
<ul class="mm-link">
<li><a href="#">Maps/Directions</a></li>
<li><a href="#">Student Health Center</a></li>
<li><a href="#"><i>More Student Resources ...</i></a></li>
</ul>
</li>

<li>
<span class="sectionhdr">GRADUATION & THE BAR</span>
<ul class="mm-link">
<li><a href="#">Bar Admissions</a></li>
<li><a href="#">Bar Review Courses</a></li>
<li><a href="#">Graduation Ceremony</a></li>
<li><a href="#">Graduation Requirements</a></li>
<li><a href="#"><i>More on Graduation & the Bar ...</i></a></li>
</ul>
</li>

</ul>
</li>
<!--BUTTON 3 END-->

<!--BUTTON 4 START-->
<li><a href="/faculty-administration/?op=0" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-facstaff.png" border="0"></a>
<ul>

<!--BUTTON 4 COLUMN 1-->
<li><span class="sectionhdr">ABOUT FACULTY</span>
<ul class="mm-link">
<li><a href="#">Faculty in the News</a></li>
<li><a href="#">Faculty Directory</a></li>
<li><a href="#">Faculty Speaker Series</a></li>
<li><a href="#">Media Inquiries for Faculty</a></li>
</ul>

<li>
<span class="sectionhdr">CALENDARS & EVENTS</span>
<ul class="mm-link">
<li><a href="#">Master Event Calendar</a></li>
<li><a href="#">List of Lectures & Academic Events at Miami Law</a></li>
<li><a href="#">Miami Law News & Events</a></li>
<li><a href="#">Plan/Publicize an Event</a></li>
<li><a href="#">Reserve a Room for an Event</a></li>
</ul>
</li>


<!--BUTTON 4 COLUMN 2-->
<li>
<span class="sectionhdr">RESOURCES/TECHNOLOGY</span>
<ul class="mm-link">
<li><a href="#">AV Services</a></li>
<li><a href="#">E-mail: Faculty & Staff</a></li>
<li><a href="#">Faculty Wiki/SSRN</a></li>
<li><a href="#">Facilities</a></li>
<li><a href="#">IT Support for Faculty & Staff</a></li>
<li><a href="#">Library Services for Faculty</a></li>
<li><a href="#">CaneLink</a></li>
</ul>
</li>

<li>
<span class="sectionhdr">DIRECTORIES</span>
<ul class="mm-link">
<li><a href="#">Faculty</a></li>
<li><a href="#">Deans and Directors</a></li>
<li><a href="#">Miami Law A-Z</a></li>
<li><a href="#">Law Librarians</a></li>
<li><a href="#">Departments</a></li>
<li><a href="#">UM Directory Search</a></li>
</ul>

</li>

<li>
<span class="sectionhdr">CLASSES</span>
<ul class="mm-link">
<li><a href="#">Academic Calendar</a></li>
<li><a href="#">Class E-mail Lists</a></li>
<li><a href="#">Course Descriptions</a></li>
<li><a href="#">First-Year Class Schedule</a></li>
<li><a href="#">Upper Level Class Schedule</a></li>
</ul>
</li>

</ul>
</li>
<!--BUTTON 4 END-->

<!--BUTTON 5 START-->

<li><a href="/alumni/?op=0" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-alumni.png" border="0"></a>
<ul>
<li><span class="sectionhdr">GIVING</span>
<ul class="mm-link">
<li><a href="#">Give to Annual Fund</a></li>
<li><a href="#">Donate Online</font></a></li>
<li><a href="#">Honor Roll of Donors</a></li>
<li><a href="#">Estate/Planned Giving</a></li>
<li><a href="#">Momentum2</a></li>
<li><a href="#">Chaplin Challenge</a></li>
<li><a href="#">Sponsorships</a></li>
</ul>
</li>

<li><span class="sectionhdr">EVENTS & NEWS</span>
<ul class="mm-link">
<li><a href="#">Homecoming 2013</a></li>
<li><a href="#">Alumni in the News</a></li>
<li><a href="#">Alumni eNewsletter</a></li>
<li><a href="#">Miami Law News Archives</a></li>
<li><a href="#">View Event Photos</a></li>
</ul>
</li>

<li><span class="sectionhdr">LEADERSHIP</span>
<ul class="mm-link">
<li><a href="#">Law Alumni Association Board of Directors</a></li>
<li><a href="#">Young Alumni Committee</a></li>
<li><a href="#">Dean's Circle & Council Committees</a></li>
<li><a href="#">Visiting Committee</a></li>
</ul>
</li>

<li><span class="sectionhdr">STAY CONNECTED</span>
<ul class="mm-link">
<li><a href="#">Update Your Address/Submit a Class Note</a></li>
<li><a href="#">Find Us On Facebook</a></li>
<li><a href="#">Find Us On LinkedIn</a></li>
<li><a href="#">Join Alumni Association</a></li>
<li><a href="#">Join Young Alumni</a></li>
<li><a href="#">Contact Alumni Office</a></li>
</ul>
</li>

<li><span class="sectionhdr">RESOURCES FOR ALUMNI</span>
<ul class="mm-link">
<li><a href="#">Order a Transcript or Diploma</a></li>
<li><a href="#">Career Services</a></li>
<li><a href="#">Library Services</a></li>
<li><a href="#">Continuing Legal Education (CLE)</a></li>
<li><a href="#">Law School Publications</a></li>
</ul>
</li>

</ul>
</li>

<!--BUTTON 5 END-->

<!--BUTTON 6 START-->

<li><a href="/career-development-office/?op=0" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-careerdevelopment.png" border="0"></a>
<ul>
<li><span class="sectionhdr">FOR STUDENTS</span>
<ul class="mm-link">
<li><a href="#">Job Search Timelines</a></li>
<li><a href="#">Individual Counseling</a></li>
<li><a href="#">Fall and Spring Recruiting</a></li>
<li><a href="#">Externship Program</a></li>
<li><a href="#">Judicial Clerkships</a></li>
</ul>
</li>

<li><span class="sectionhdr">&nbsp;</span>
<ul class="mm-link">
<li><a href="#">Preparing Job Search Materials & Strategies</a></li>
<li><a href="#">Legal Corps</a></li>
<li><a href="#">Symplicity-Job Posting</a></li>
<li><a href="#"><i>More for Students</i></a></li>
</ul>
</li>

<li><span class="sectionhdr">FOR ALUMNI</span>
<ul class="mm-link">
<li><a href="#">Access Job Postings</a></li>
<li><a href="#">Alumni Job Guide</a></li>
<li><a href="#">Reciprocity with Other Schools</a></li>
</ul>
</li>

<li><span class="sectionhdr">&nbsp;</span>
<ul class="mm-link">
<li><a href="#">Networking Guide</a></li>
<li><a href="#">Obtain a Transcript</a></li>
<li><a href="#"><i>More Alumni Resources</i></a></li>
</ul>
</li>

<li><span class="sectionhdr">FOR EMPLOYERS</span>
<ul class="mm-link">
<li><a href="#">Submit a Job Posting</a></li>
<li><a href="#">Receive Student Resumes</a></li>
<li><a href="#">Interview Our Students</a></li>
<li><a href="#">Participate In Our Externship Program</a></li>
<li><a href="#">Become a Legal Corps Host Organization</a></li>
</ul>
</li>

</ul>
</li>

<!--BUTTON 6 END-->

<!--BUTTON 7 START-->

<li><a href="/library/?op=0" class="btn-mainnav"><img src="http://www.law.miami.edu/_ui/images/btn-library.png" border="0"></a>
<ul>
<li><span class="sectionhdr">LIBRARY SERVICES</span>
<ul class="mm-link">
<li><a href="#">Law Faculty</a></li>
<li><a href="#">Law Students</a></li>
<li><a href="#">Law Reviews</a></li>
<li><a href="#">Law Alumni</a></li>
<li><a href="#">Public</a></li>
</ul>
</li>

<li><span class="sectionhdr">RESEARCH TOOLS</span>
<ul class="mm-link">
<li><a href="#">Law Library Catalog</a></li>
<li><a href="#">Research Databases</a></li>
<li><a href="#">Free Internet Resources</a></li>
<li><a href="#">Research Guides</a></li>
<li><a href="#"><i>More ...</i></a></li>
</ul>
</li>

<li><span class="sectionhdr">QUICK LINKS</span>
<ul class="mm-link">
<li><a href="#">Bloomberg BNA</a></li>
<li><a href="#">CALI</a></li>
<li><a href="#">Congressional (ProQuest)</a></li>
<li><a href="#">Hein Online</a></li>
<li><a href="#">KluwerArbitration</a></li>
<li><a href="#">Lexis</a></li>
<li><a href="#">WestLaw</a></li>
<li><a href="#">Off-Campus Wireless Access</a></li>
<li><a href="#"><i>More ...</i></a></li>
</ul>
</li>

<li><span class="sectionhdr">GENERAL INFORMATION</span>
<ul class="mm-link">
<li><a href="#">Ask-a-Librarian</a></li>
<li><a href="#">Library Hours</a></li>
<li><a href="#">Training Schedule</a></li>
<li><a href="#">Staff Directory</a></li>
<li><a href="#">Maps of the Library</a></li>
</ul>
</li>

<li><span class="sectionhdr">&nbsp;</span>
<ul class="mm-link">
<li><a href="#">Directions</a></li>
<li><a href="#">Forms and Policies</a></li>
<li><a href="#">About the Library</a></li>
<li><a href="#"><i>More ...</i></a></li>
</ul>
</li>

</ul>
</li>

</ul>

<!--BUTTON 7 END-->

</div>
</div>