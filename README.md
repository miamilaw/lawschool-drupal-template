MiamiLaw Drupal Template
========================

Vagrant
-------

  * To setup drupal locally with vagrant
  * Make sure you have installed virtualbox and vagrant
  * cd into vagrant directory and type: vagrant up
    * Vagrant commands:
    * vagrant up - Brings up the box and provisions it with LAMP and drupal (note this will bring up the box each time and provision it add --no-provision to just bring it up)
    * vagrant halt - Shuts down the vm (halt)
    * vagrant suspend - Suspends the machine state
    * vagrant resume - Resumes the machine state
  * The website will be hosted on http://192.168.33.10
    * login: admin
    * password: drupaladmin

Drupal
------
  * Version 7.26
  * Go to Appearance and choose the Law School Template and set as default
  * Enjoy

Miscellaneous
-------------
  * https://drupal.org/documentation/theme
  * http://themery.com/dgd7
