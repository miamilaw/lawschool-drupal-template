name                'Drupal based settings for law school template'
maintainer          'webdev'
maintainer_email    'noreply@law.miami.edu'
license             'bsd'
description         'Setup template development'

depends 'drupal'
