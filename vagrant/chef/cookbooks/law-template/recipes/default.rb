packages = %w{wget python-dev git subversion php-net-ldap}

packages.each do |pkg|
  package pkg do
    action :install
  end
end

bash "symlink" do
  cwd '/home/vagrant'
  code <<-EOH
    ln -s /home/vagrant/drupal /var/www/drupal/sites/all/themes/lawschool
  EOH
end

bash "sites-ssl" do
  cwd '/etc/apache2'
  code <<-EOH
    cp sites-available/default-ssl sites-enabled/000-default-ssl
  EOH
end
